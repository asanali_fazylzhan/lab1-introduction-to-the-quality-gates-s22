sudo apt update
sudo apt install $DOCKER_IO -y
sudo docker login -u "$DOCKERHUB_LOGIN" -p "$DOCKERHUB_TOKEN" $DOCKER_IO
sudo kill $(ps aux | grep java | grep -v 'grep' | awk '{print $2}')
sudo docker run -p 8080:8080 -d "$DOCKER_IO_IMAGE"
